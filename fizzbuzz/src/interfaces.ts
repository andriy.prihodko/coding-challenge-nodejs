export interface BuzzRule {
  rule: (num: number) => void
  output: string
}
