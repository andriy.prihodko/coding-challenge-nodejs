import {pyramid} from "./index";

describe('pyramid function', () => {
  let consoleSpy;

  beforeEach(() => {
    consoleSpy = jest.spyOn(console, 'log').mockImplementation(() => {});
  })

  afterEach(() => {
    consoleSpy.mockRestore();
  })

  test('renders 1 star pyramid', () => {
    pyramid(1);
    expect(consoleSpy).toHaveBeenCalledWith('*');
  });

  test('renders 3 stars pyramid', () => {
    pyramid(3);
    expect(consoleSpy).toHaveBeenCalledTimes(3);
    expect(consoleSpy.mock.calls[0][0]).toBe('  *');
    expect(consoleSpy.mock.calls[1][0]).toBe(' ***');
    expect(consoleSpy.mock.calls[2][0]).toBe('*****');
  });

  test('renders 5 stars pyramid', () => {
    pyramid(5);

    expect(consoleSpy).toHaveBeenCalledTimes(5);
    expect(consoleSpy.mock.calls[0][0]).toBe('    *');
    expect(consoleSpy.mock.calls[1][0]).toBe('   ***');
    expect(consoleSpy.mock.calls[2][0]).toBe('  *****');
    expect(consoleSpy.mock.calls[3][0]).toBe(' *******');
    expect(consoleSpy.mock.calls[4][0]).toBe('*********');
  });

  test('renders 10 stars pyramid', () => {
    pyramid(10);

    expect(consoleSpy).toHaveBeenCalledTimes(10);
    expect(consoleSpy.mock.calls[0][0]).toBe('         *');
    expect(consoleSpy.mock.calls[1][0]).toBe('        ***');
    expect(consoleSpy.mock.calls[2][0]).toBe('       *****');
    expect(consoleSpy.mock.calls[3][0]).toBe('      *******');
    expect(consoleSpy.mock.calls[4][0]).toBe('     *********');
    expect(consoleSpy.mock.calls[5][0]).toBe('    ***********');
    expect(consoleSpy.mock.calls[6][0]).toBe('   *************');
    expect(consoleSpy.mock.calls[7][0]).toBe('  ***************');
    expect(consoleSpy.mock.calls[8][0]).toBe(' *****************');
    expect(consoleSpy.mock.calls[9][0]).toBe('*******************');
  });
});
