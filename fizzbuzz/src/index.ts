/**
 *
 * Given is the following FizzBuzz application which counts from 1 to 100 and outputs either the
 * corresponding number or the corresponding text if one of the following rules apply.
 *
 * Rules:
 *  - dividable by 3 without a remainder -> Fizz
 *  - dividable by 5 without a remainder -> Buzz
 *  - dividable by 3 and 5 without a remainder -> FizzBuzz
 *
 * ACCEPTANCE CRITERIA:
 *
 * Please refactor this code so that it can be easily extended with other rules, such as
 * - "if it is dividable by 7 without a remainder output Bar"
 * - "if multiplied by 10 is larger than 100 output Foo"
 */
import {BuzzRule} from "./interfaces";

export const dividableBy = (divisor: number) => (num: number): boolean => num % divisor === 0;

export const defaultRules: Array<BuzzRule> = [
  {rule: dividableBy(3), output: 'Fizz'},
  {rule: dividableBy(5), output: 'Buzz'},
];

export const fizzbuzz = (limit: number = 100, rules: Array<BuzzRule> = defaultRules) => {
  const buzz = Array(limit).fill('').reduce((acc, _, i) => {
    const matchingRules = rules.filter(item => item.rule(i + 1));
    const output = matchingRules.length ? matchingRules.map(rule => rule.output).join("") : i + 1;

    return [...acc, `${i + 1}: ${output}`];
  }, []);

  console.log(buzz.join('\n'))
};

console.info('Default rules:')
fizzbuzz()

console.info('Extended rules:')
const rules = [
  ...defaultRules,
  {rule: dividableBy(7), output: "Bar"},
  {rule: (num: number) => num * 10 > 100, output: "Foo"},
];
fizzbuzz(100, rules)
