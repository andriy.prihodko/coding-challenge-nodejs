/**

 Print a pyramid!

 *
 ***
 *****
 *******
 *********

 ACCEPTANCE CRITERIA:

 Write a script to output pyramid of given size to the console (with leading spaces).

 */
export const pyramid = (size = 5) => {
  for (let i = 1; i <= size; i++) {
    const spaces = ' '.repeat(size - i);
    const stars = '*'.repeat(2 * i - 1);

    console.log(spaces + stars);
  }
};

pyramid(5)
