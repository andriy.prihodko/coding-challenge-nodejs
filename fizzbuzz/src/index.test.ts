import {defaultRules, dividableBy, fizzbuzz} from "./index";

describe("fizzbuzz function", () => {
  let consoleSpy;

  beforeEach(() => {
    consoleSpy = jest.spyOn(console, 'log').mockImplementation(() => {});
  })

  afterEach(() => {
    consoleSpy.mockRestore();
  })

  describe('with defaults', () => {
    it("outputs the correct results", () => {
      const expected = [
        "1: 1",
        "2: 2",
        "3: Fizz",
        "4: 4",
        "5: Buzz",
        "6: Fizz",
        "7: 7",
        "8: 8",
        "9: Fizz",
        "10: Buzz",
        "11: 11",
        "12: Fizz",
        "13: 13",
        "14: 14",
        "15: FizzBuzz"
      ].join('\n');

      fizzbuzz(15);

      expect(consoleSpy.mock.calls.flat()[0]).toEqual(expected);
    });
  });

  describe('with custom limit / rules', () => {
    it("outputs the correct result", () => {
      const expected = [
        "1: 1",
        "2: 2",
        "3: Fizz",
        "4: 4",
        "5: Buzz",
        "6: Fizz",
        "7: Bar",
        "8: 8",
        "9: Fizz",
        "10: Buzz",
        "11: Foo",
        "12: FizzFoo",
        "13: Foo",
        "14: BarFoo",
        "15: FizzBuzzFoo",
        "16: Foo",
        "17: Foo",
        "18: FizzFoo",
        "19: Foo",
        "20: BuzzFoo",
        "21: FizzBarFoo",
        "22: Foo",
        "23: Foo",
        "24: FizzFoo",
        "25: BuzzFoo"
      ].join('\n');
      const rules = [
        ...defaultRules,
        {rule: dividableBy(7), output: "Bar"},
        {rule: (num) => num * 10 > 100, output: "Foo"},
      ];

      fizzbuzz(25, rules);

      expect(consoleSpy.mock.calls.flat()[0]).toEqual(expected);
    });
  });
});
